25L Pineapple Beer

What you'll need: -

25L Clean Bucket, sealable.
Make a pressure vent, use a ballon, small piece of hose. (Need to let gas out, not in)
About 20L pre-boiled water (25-30 deg C)
1 kg sugar, disolved in boiled water (Play around, more is stronger)
1 pkt yeast/ teaspoon recovered yeast (Play around, pop that lid off).
2/3 Pineapples / Apples / Oranges, make your own mix here, about 1kg of fruit.

Mix it all up, fruit, dissovled yeast/sugar starter etc in the bucket.
Lid and leave for about 7 days, longer the better, stir it up daily, keep at between 25-28degC (We just put it outside under cover).

Drain liquids off, use a fine sieve or something, place in bottles, close, leave for an hour or so.
Decant liquid again, throwing out the sediment.
Leave in fridge. (Store in cool place, maybe add a half teaspoon sugar to get some extra carbonation.

Mix 50/50 with sprite/lemonade or something (To taste, it's a bitter beer)

Keep the small bits of sediment left at the bottom. (This is your yeast, just let it settle in a bottle in the fridge, use in another brew if you want)

Distill it if you want too.

ABV +- 5% after 7-9 days from a previous 5L batch we made.
 
